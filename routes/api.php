<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\xmlController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('perrito', function () {
//     return file_get_contents('https://cfdisat.blob.core.windows.net/lco?restype=container&comp=list&prefix=LCO_2018-06-22');;
// });

Route::get('/getXml/{date}', [xmlController::class, 'newXml']);
