<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Contracts\Routing\ResponseFactory;

class xmlController extends Controller
{
    /**
    * @author 
    * @version 1.0.0
    * @api
    * @param  string JSON
    * @return string JSON
    */
    public function newxml(request $request)
    {
        try {
            $xmlString = file_get_contents('https://cfdisat.blob.core.windows.net/lco?restype=container&comp=list&prefix=LCO_'.$request->date);
            $xmlObject = simplexml_load_string($xmlString);
            $json = json_encode($xmlObject);
            $result = json_decode($json, true);
            $this->firstExtraction($result);
            // dd($phpArray);

            $retval = array(
                'code' => 200,
                'message' => 'Se creó Json',
                'data' => $result,
            );
            return $retval;
        } catch (\Exception $ex) {
            Log::debug("ErrMsg: " .$ex->getMessage()." File: ".$ex->getFile()." Line: ".$ex->getLine() );
            $retval = array(
                'code' => $ex->getCode(),
                'message' => $ex->getMessage(),
                'data' => [],
            );
            return response()->json($retval, $ex->getCode());
        }
    }

    public function firstExtraction($json)
    {
        foreach ($json['Blobs']['Blob'] as $key => $dia) {
            $urls[$key] = $dia['Url'];
        }

        return $this->secondExtraction($urls);
        
    }

    public function secondExtraction($urls)
    {
        foreach ($urls as $key => $url) {
            dd($this->getDownload($url));
            // var_dump($url);exit;
            $xmlString = file_get_contents($url);
            $xmlObject = simplexml_load_string($xmlString);
            $json = json_encode($xmlObject);
            $result = json_decode($json, true);
            // $datos[$key] = 
            dd($result);
        }
    }

    public function getDownload($url){
        return response()->file($url);
    }
}
